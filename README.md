# @dimensional-innovations/vue-camera

vue-camera builds on the [mediaDevices API](https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices) offered by the browser to integrate a web camera with Vue applications.

## Getting Started

### Install

Add the package using yarn or npm:
```
yarn add @dimensional-innovations/vue-camera
```
or
```
npm install --save @dimensional-innovations/vue-camera
```

### Usage

There are two pieces from this package that you'll need to get started. The useCamera method takes a deviceId and returns the MediaStream that represents the device's view. The Camera component can then be used to show this view in a responsive layout. For example:

``` vue
<script lang="ts">
import { Camera, useCamera, useCamerasList } from '@dimensional-innovations/vue-camera';
import { defineComponent, ref, watch } from 'vue';

export default defineComponent({
  components: {
    Camera
  },
  setup () {
    const devices = useCamerasList();
    const selectedDevice = ref<string>();
    const mediaStream = useCamera(selectedDevice);

    return {
      devices,
      selectedDevice,
      mediaStream
    };
  }
});
</script>

<template>
  <div style="position: relative">
    <camera :mediaStream="mediaStream" />
    <div style="position: absolute; top: 0px; right: 0px;">
      <select v-model="selectedDevice">
        <option :value="undefined">
          Select a camera
        </option>
        <option v-for="device in devices" :key="device.deviceId" :value="device.deviceId">
          {{ device.label }}
        </option>
      </select>
    </div>
  </div>
</template>
```

Additional methods and components that can be used to manage a camera are exported from this package as well. For the complete api this package exposes see the [API](/API.md) doc.


