import * as remote from '@electron/remote/main';
import installExtension, { VUEJS3_DEVTOOLS } from 'electron-devtools-installer';
import { app, BrowserWindow, protocol } from 'electron';
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib';

let browserWindow: BrowserWindow | null = null;

(async function init() {
  remote.initialize();
  protocol.registerSchemesAsPrivileged(
    [
      {
        scheme: 'app',
        privileges: { secure: true, standard: true, supportFetchAPI: true },
      },
    ],
  );

  await app.whenReady();

  browserWindow = new BrowserWindow({
    height: 1080,
    width: 1920,
    backgroundColor: '#000',
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true,
      webSecurity: false
    }
  });

  // open the dev server url if it's available (if the app is running in dev mode)
  if (process.env.WEBPACK_DEV_SERVER_URL) {
    await installExtension(VUEJS3_DEVTOOLS);
  }

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    await browserWindow.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
    browserWindow.webContents.openDevTools();
  } else {
    createProtocol('app');
    browserWindow.loadURL('app://./index.html');
  }

  browserWindow.on('closed', () => browserWindow = null);
  process.on('message', (data) => {
    if (data === 'graceful-exit') app.quit();
  });
  process.on('SIGTERM', app.quit);
  app.commandLine.appendSwitch('touch-events', 'enabled');
  app.on('window-all-closed', app.quit);
})();
