import { useDevicesList } from '@vueuse/core';
import { Ref } from 'vue';

/**
 * Gets a reactive list of devices that support video.
 * 
 * @returns - The list of devices that can be used as cameras.
 */
export function useCamerasList(): Ref<Array<MediaDeviceInfo>> {
  const { videoInputs } = useDevicesList();
  return videoInputs;
}