import { get, isDefined, set } from '@vueuse/core';
import first from 'lodash.first';
import pick from 'lodash.pick';
import { Pane } from 'tweakpane';
import { defineComponent, h, reactive, ref, watch, WatchStopHandle } from 'vue';
import { applyConstraints } from './util';

/**
 * Component that allows a user to changes settings on a connected camera device,
 * emit an event each time the user does with the current settings.
 */
export const CameraSettings = defineComponent({
  name: 'CameraSettings',
  props: {
    mediaStream: {
      type: MediaStream,
      required: false
    }
  },
  emits: ['update'],
  setup(props, { emit }) {
    const pane = ref<Pane>();
    const container = ref<HTMLDivElement>();
    const watchHandles: Array<WatchStopHandle> = [];

    watch([() => props.mediaStream, container], ([mediaStream, container]) => {
      for (const handle of watchHandles) {
        handle();
      }
      watchHandles.length = 0;
      
      if (isDefined(pane)) {
        get(pane).dispose();
        set(pane, undefined);
      }

      if (!container || !mediaStream) {
        return;
      }

      const videoTrack = first(mediaStream.getVideoTracks());
      if (!videoTrack) {
        return;
      }

      const settings = reactive(pick(videoTrack.getSettings(), 'brightness', 'colorTemperature', 'contrast', 'focusDistance', 'focusMode', 'saturation', 'sharpness', 'whiteBalanceMode', 'zoom'));
      const capabilities = videoTrack.getCapabilities();

      const _pane = new Pane({ container: container });
      set(pane, _pane);

      const cameraSettingsFolder = _pane.addFolder({ title: 'Camera Settings' });

      if (capabilities.brightness) {
        const brightnessInput = cameraSettingsFolder.addInput(settings, 'brightness', {
          min: capabilities.brightness.min,
          max: capabilities.brightness.max,
          step: capabilities.brightness.step
        });
        brightnessInput.on('change', (ev) => applyConstraints(mediaStream, { brightness: ev.value }));
      }

      if (capabilities.colorTemperature) {
        const colorTemperatureInput = cameraSettingsFolder.addInput(settings, 'colorTemperature', {
          min: capabilities.colorTemperature.min,
          max: capabilities.colorTemperature.max,
          step: capabilities.colorTemperature.step
        });
        colorTemperatureInput.on('change', (ev) => applyConstraints(mediaStream, { colorTemperature: ev.value }));
        const handle = watch(() => settings.whiteBalanceMode, (whiteBalanceMode) => colorTemperatureInput.disabled = whiteBalanceMode !== 'manual', { immediate: true });
        watchHandles.push(handle);
      }

      if (capabilities.contrast) {
        const contrastInput = cameraSettingsFolder.addInput(settings, 'contrast', {
          min: capabilities.contrast.min,
          max: capabilities.contrast.max,
          step: capabilities.contrast.step
        });
        contrastInput.on('change', (ev) => applyConstraints(mediaStream, { contrast: ev.value }));
      }

      if (capabilities.focusDistance) {
        const focusDistanceInput = cameraSettingsFolder.addInput(settings, 'focusDistance', {
          min: capabilities.focusDistance.min,
          max: capabilities.focusDistance.max,
          step: capabilities.focusDistance.step
        });
        focusDistanceInput.on('change', (ev) => applyConstraints(mediaStream, { focusDistance: ev.value }));
        const handle = watch(() => settings.focusMode, (focusMode) => focusDistanceInput.disabled = focusMode !== 'manual', { immediate: true });
        watchHandles.push(handle);
      }

      if (capabilities.focusMode) {
        const focusModeInput = cameraSettingsFolder.addInput(settings, 'focusMode', {
          options: capabilities.focusMode.reduce((options, value) => {
            options[value] = value;
            return options;
          }, {} as Record<string, string>)
        });
        focusModeInput.on('change', (ev) => applyConstraints(mediaStream, { focusMode: ev.value }));
      }

      if (capabilities.saturation) {
        const saturationInput = cameraSettingsFolder.addInput(settings, 'saturation', {
          min: capabilities.saturation.min,
          max: capabilities.saturation.max,
          step: capabilities.saturation.step
        });
        saturationInput.on('change', (ev) => applyConstraints(mediaStream, { saturation: ev.value }));
      }

      if (capabilities.sharpness) {
        const sharpnessInput = cameraSettingsFolder.addInput(settings, 'sharpness', {
          min: capabilities.sharpness.min,
          max: capabilities.sharpness.max,
          step: capabilities.sharpness.step
        });
        sharpnessInput.on('change', (ev) => applyConstraints(mediaStream, { sharpness: ev.value }));
      }

      if (capabilities.whiteBalanceMode) {
        const whiteBalanceModeInput = cameraSettingsFolder.addInput(settings, 'whiteBalanceMode', {
          options: capabilities.whiteBalanceMode.reduce((options, value) => {
            options[value] = value;
            return options;
          }, {} as Record<string, string>)
        });
        whiteBalanceModeInput.on('change', (ev) => applyConstraints(mediaStream, { whiteBalanceMode: ev.value }));
      }

      if (capabilities.zoom) {
        const zoomInput = cameraSettingsFolder.addInput(settings, 'zoom', {
          min: capabilities.zoom.min,
          max: capabilities.zoom.max,
          step: capabilities.zoom.step
        });
        zoomInput.on('change', (ev) => applyConstraints(mediaStream, { zoom: ev.value }));
      }

      const handle = watch(settings, (settings) => emit('update', settings), { immediate: true });
      watchHandles.push(handle);
    }, { immediate: true });

    return () => h('div', { ref: container, class: 'camera-settings' });
  }
});
